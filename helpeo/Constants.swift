//
//  Constants.swift
//  helpeo
//
//  Created by Ilhan Seven on 05/08/16.
//  Copyright © 2016 egas. All rights reserved.
//

import Foundation
import UIKit

let SHADOW_COLOR: CGFloat = 157.0 / 255.0

// Keys
let KEY_UID = "uid"

// Segues
let SEGUE_LOGGED_IN = "loggedIn"
let SEGUE_PROFILE_SETUP = "profileSetup"
let SEGUE_PROFILE_SETUP_DONE = "oneTimeProfileSetupDone"

// Status Codes
let STATUS_ACCOUNT_NONEXIST = 17011