//
//  Profile.swift
//  helpeo
//
//  Created by Ilhan Seven on 11/08/16.
//  Copyright © 2016 egas. All rights reserved.
//

// THIS CLASS BECAME USELESS AFTER I LEARNED NSUSERDEFAULTS. BUT WILL BE HERE AS IT CAN BE NEEDED IN THE FUTURE.

import Foundation
import Firebase

class Profile {
    
    private var _userName: String!
    private var _name: String!
    private var _surname: String!
    private var _photoUrl: String?
    private var _userKey: String!
    private var _userRef: FIRDatabaseReference!
    
    var userName: String {
        return _userName!
    }
    
    var name: String {
        return _name
    }
    
    var surname: String {
        return _surname
    }
    
    var userKey: String {
        return _userKey
    }
    
    var photoUrl: String? {
        return _photoUrl
    }

    init(username: String, name: String, surname: String) {
        self._userName = username
        self._name = name
        self._surname = surname
    }
    
    
    func fillInUserData(username: String, name: String, surname: String, photoUrl: String?) {
        
        _userName = username
        _name = name
        _surname = surname
        _photoUrl = photoUrl
    }
    
    func updateUsername(usrName: String) {
        _userName = usrName
        
        let userNameRef = DataService.ds.REF_USER_CURRENT.child("username")
        userNameRef.setValue(_userName)
    }
    
    func setImageUrl(imgUrl: String) {
        _photoUrl = imgUrl
    }
}