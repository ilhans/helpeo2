//
//  xViewController.swift
//  helpeo
//
//  Created by Ilhan Seven on 16/08/16.
//  Copyright © 2016 egas. All rights reserved.
//


// to do: If user is signed up via facebook. Basic profile data and profile picture should be pulled from facebook.

import UIKit
import Firebase

class ProfileSetupViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: MaterialTextField!
    @IBOutlet weak var surname: MaterialTextField!
    @IBOutlet weak var username: MaterialTextField!
    @IBOutlet weak var skillsEntry: UITextView!
    @IBOutlet weak var tapToUploadText: UILabel!

    var imagePicker: UIImagePickerController!
    static var imageCache = NSCache()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
    }

    @IBAction func profileSetupDone(sender: AnyObject) {
        
        if let userName = username.text where username.text != "" {
            
            DataService.ds.REF_USERS.observeSingleEventOfType(.Value, withBlock: { snapshot in
                
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    
                    var usernameCandidateIsUnique = true
                    
                    for snap in snapshots {
                        
                        if snap.hasChild("username") {
                            
                            if let postDic = snap.value as? Dictionary<String, AnyObject> {
                                
                                if let snapUsername = postDic["username"] as? String {
                                    
                                    if userName == snapUsername {
                                        
                                        usernameCandidateIsUnique = false
                                        self.showErrorAlert("Username already exists", msg: "Please pick another")
                                    }
                                }
                            }
                        }
                    }
                    
                    if usernameCandidateIsUnique {
                        
                        if let Name = self.name.text, Surname = self.surname.text where self.name.text != "" && self.surname.text != "" {
                            
                            if let img = self.profileImage.image where self.profileImage.image != UIImage(named: "empty_profile") {
                                
                                let imgData = UIImageJPEGRepresentation(img, 0.2)!
                                let imgPath = "\(NSDate.timeIntervalSinceReferenceDate())"
                                let metadata = FIRStorageMetadata()
                                metadata.contentType = "image/jpg"
                                
                                DataService.ds.REF_IMAGES.child(imgPath).putData(imgData, metadata: metadata, completion: { metadata, error in
                                    
                                    if error != nil {
                                        
                                        print("Error uploading image")
                                    } else {
                                        
                                        if let meta = metadata {
                                            
                                            if let imgLink = meta.downloadURL()?.absoluteString {
                                                
                                                print("Image uploaded successfully! Link: \(imgLink)")
                                                self.postToFirebase(imgLink)
                                                let dict = ["username" : userName, "name" : Name, "surname" : Surname, "photoUrl": imgLink]
                                                NSUserDefaults.standardUserDefaults().setValuesForKeysWithDictionary(dict)
                                            }
                                        }
                                    }
                                })
                            } else {

                                let dict = ["username" : userName, "name" : Name, "surname" : Surname]
                                NSUserDefaults.standardUserDefaults().setValuesForKeysWithDictionary(dict)
                                self.postToFirebase(nil)
                            }
    
                            self.performSegueWithIdentifier(SEGUE_PROFILE_SETUP_DONE, sender: nil)
                            
                        } else {
                            self.showErrorAlert("Sorry", msg: "name and surname fields cannot be empty")
                        }
                    }
                }
            })
        } else {
            self.showErrorAlert("userName cannot be empty", msg: "Pick a username")
        }
        
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        profileImage.image = image.circle
        tapToUploadText.hidden = true
    }
    
    @IBAction func uploadProfilePhoto(sender: UITapGestureRecognizer) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }

    func showErrorAlert(title: String, msg: String) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let action = UIAlertAction(title: "Ok", style: .Default, handler: nil)
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func postToFirebase(imgUrl: String?) {
        
        var post: Dictionary<String, AnyObject> = [ "username": username.text!, "name": name.text!, "surname": surname.text!]
        if imgUrl != nil {
            print("yes")
            post["photoUrl"] = imgUrl!
        }
        
        let firebasePost = DataService.ds.REF_USER_CURRENT
        firebasePost.updateChildValues(post)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
