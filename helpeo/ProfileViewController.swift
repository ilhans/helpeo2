//
//  SecondViewController.swift
//  helpeo
//
//  Created by Ilhan Seven on 05/08/16.
//  Copyright © 2016 egas. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var tapToChangePic: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var enterUsernameField: MaterialTextField!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!

    var request: Request!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        profilePic.layer.cornerRadius = profilePic.frame.size.width / 2
        profilePic.clipsToBounds = true
        
        if let username = NSUserDefaults.standardUserDefaults().valueForKey("username") as? String {
            
            self.userNameLbl.text = username
        }

        
        if let imgLink = NSUserDefaults.standardUserDefaults().valueForKey("photoUrl") as? String {
            
            request = Alamofire.request(.GET, imgLink).validate(contentType: ["image/*"]).response(completionHandler: { request, response, data, err in
                
                if err == nil {
                    if let img = UIImage(data: data!) {
                        self.profilePic.image = img
                    }
                }
            })
            
        }
        /*
        DataService.ds.REF_USER_CURRENT.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    
                    if snap.hasChild("username") {
                        
                        if let postDic = snap.value as? Dictionary<String, AnyObject> {
                            
                            if let snapUsername = postDic["username"] as? String {
                    }
                    
                }
    
            if let snap = snapshot.value as? String {
                self.profile = Profile(username: snap, name: "osman", surname: "hasdemir")
                self.userNameLbl.text = self.profile.userName
                self.enterUsernameField.placeholder = self.profile.userName
            }
        })*/
    }
    
    @IBAction func editProfilePressed(sender: UIButton) {
        tapToChangePic.hidden = false
        enterUsernameField.hidden = false
        userNameLbl.hidden = true
        editBtn.hidden = true
        okBtn.hidden = false
        
    }
    
    @IBAction func okEditingProfilePressed(sender: AnyObject) {
        tapToChangePic.hidden = true
        enterUsernameField.hidden = true
        userNameLbl.hidden = false
        editBtn.hidden = false
        okBtn.hidden = true
        
        if let usrNameEntry = enterUsernameField.text where usrNameEntry != "" {
//            self.profile.updateUsername(usrNameEntry)
//            userNameLbl.text = profile.userName
//            enterUsernameField.placeholder = profile.userName
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Should actually log user out.
    @IBAction func logOut(sender: AnyObject) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyBoard.instantiateViewControllerWithIdentifier("LoginVC") as! LoginViewController
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = loginVC
    }
    
    func updateUserName() {
        
    }
}

